import React from 'react';
import { GameLevelType } from '../../types';
import './Header.css';

interface Props {
  level: GameLevelType;
}

function Header({ level }: Props) {
  return (
    <>
      <header className="header">
        <h1>
          <span>🧠</span> Memory Game
        </h1>
        {level && <a href="/">Back to menu</a>}
      </header>
      {level && <h3>level: {(level as string).toLocaleUpperCase()}</h3>}
    </>
  );
}

export default Header;
