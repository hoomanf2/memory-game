import React from 'react';
import Card from '../Card/Card';
import './Board.css';

interface Props {
  cards: number[];
  cardStatus: 'isPlaying' | 'disabled' | null;
  initialSelectedCards: number[];
  selectCard(cardNumber: number): void;
}

function Board({ cards, cardStatus, initialSelectedCards, selectCard }: Props) {
  const isCardSelectable = (cardNumber: number) =>
    cardStatus === 'isPlaying'
      ? initialSelectedCards.includes(cardNumber)
      : true;

  return (
    <div className="board">
      {cards.map((cardNumber: number) => (
        <Card
          key={cardNumber}
          cardNumber={cardNumber}
          cardStatus={cardStatus}
          selectable={isCardSelectable(cardNumber)}
          onClick={selectCard}
        />
      ))}
    </div>
  );
}

export default React.memo(Board);
