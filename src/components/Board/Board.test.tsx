import React from 'react';
import { render, screen } from '@testing-library/react';
import Board from './Board';

test('renders boards with numbers', async () => {
  const cards = [];

  for (let i = 0; i < 48; i++) {
    cards.push(i);
  }

  render(
    <Board
      cards={cards}
      cardStatus={null}
      initialSelectedCards={[]}
      selectCard={() => null}
    />
  );

  const CardElements = screen.queryAllByTestId('card');
  expect(CardElements.length).toBe(48);
});
