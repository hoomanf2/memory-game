import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import Card from './Card';

test('renders a card with card number', async () => {
  render(
    <Card
      cardNumber={7}
      cardStatus={null}
      selectable={true}
      onClick={() => null}
    />
  );

  const cardNumberElement = screen.getByTestId('cardNumber');
  expect(cardNumberElement.innerHTML).toBe('7');
});

test('renders a card with disabled status', async () => {
  render(
    <Card
      cardNumber={15}
      cardStatus={'disabled'}
      selectable={true}
      onClick={() => null}
    />
  );

  const cardElement = screen.getByTestId('card');
  expect(cardElement).toHaveClass('disabled');
});

test('renders a card with isPlaying status and imitate click event', async () => {
  render(
    <Card
      cardNumber={16}
      cardStatus={'isPlaying'}
      selectable={true}
      onClick={() => null}
    />
  );

  const cardElement = screen.getByTestId('card');

  const leftClick = { button: 0 };
  fireEvent.click(cardElement, leftClick);

  expect(cardElement).toHaveClass('flipped');
});

test('renders a card with disabled status and imitate click event', async () => {
  render(
    <Card
      cardNumber={16}
      cardStatus={'disabled'}
      selectable={true}
      onClick={() => null}
    />
  );

  const cardElement = screen.getByTestId('card');

  const leftClick = { button: 0 };
  fireEvent.click(cardElement, leftClick);

  expect(cardElement).not.toHaveClass('flipped');
});

test('renders a card that is not selectable', async () => {
  render(
    <Card
      cardNumber={16}
      cardStatus={'isPlaying'}
      selectable={false}
      onClick={() => null}
    />
  );

  const cardElement = screen.getByTestId('card');

  const leftClick = { button: 0 };
  fireEvent.click(cardElement, leftClick);

  expect(cardElement).toHaveClass('flipped');

  await waitFor(() => {
    expect(cardElement).not.toHaveClass('flipped');
  });
});
