import React, { useEffect, useState } from 'react';
import './Card.css';

interface Props {
  cardNumber: number;
  cardStatus: 'isPlaying' | 'disabled' | null;
  selectable: boolean;
  onClick: (cardNumber: number) => void;
}

function Card({
  cardNumber,
  cardStatus,
  selectable,
  onClick: onClickCard
}: Props) {
  const [flipped, setFlipped] = useState(false);
  const [disabled, setDisabled] = useState(false);

  const resetCard = () => {
    setFlipped(false);
    setDisabled(false);
  };

  useEffect(() => {
    switch (cardStatus) {
      case 'isPlaying':
        resetCard();
        break;
      case 'disabled':
        setDisabled(true);
        break;
      default:
        resetCard();
    }
  }, [cardStatus]);

  const selectCard = (cardNumber: number) => {
    if (!disabled) {
      setFlipped(true);
      if (cardStatus !== 'isPlaying' || selectable) {
        onClickCard(cardNumber);
      } else {
        setTimeout(() => setFlipped(false), 500);
      }
    }
  };

  return (
    <div
      className={`card${flipped ? ' flipped' : ''}${
        disabled ? ' disabled' : ''
      }`}
      onClick={() => selectCard(cardNumber)}
      data-testid="card"
    >
      <div className="card-inner">
        <div className="card-front"></div>
        <div className="card-back">
          <h2 data-testid="cardNumber">{cardNumber}</h2>
        </div>
      </div>
    </div>
  );
}

export default React.memo(Card);
