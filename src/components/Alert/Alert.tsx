import React from 'react';
import './Alert.css';

interface Props {
  type: 'info' | 'danger' | 'success';
  children: any;
}

function Alert({ type, children }: Props) {
  return <div className={`alert ${type}`}>{children}</div>;
}

export default React.memo(Alert);
