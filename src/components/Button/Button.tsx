import React from 'react';
import './Button.css';

interface Props {
  children: string;
  onClick(): any;
}

function button({ children, onClick: onClickButton, ...rest }: Props) {
  return (
    <button className="button" onClick={onClickButton} {...rest}>
      {children}
    </button>
  );
}

export default button;
