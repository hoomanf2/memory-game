import React from 'react';
import { History as HistoryObject } from '../../types';

interface Props {
  history: HistoryObject[];
}

function History({ history }: Props) {
  return (
    <div className="history">
      <h3>History</h3>
      <ul data-testid="historyList">
        {!history.length ? (
          <li>There is no history to show</li>
        ) : (
          history.map((record, index) => (
            <li key={index}>
              {`Status: ${
                record.isWinning ? 'Win' : 'Lose'
              } | Selected Cards: ${record.selectedCards.toString()} | Date: ${
                record.dateTime
              }`}
            </li>
          ))
        )}
      </ul>
    </div>
  );
}

export default React.memo(History);
