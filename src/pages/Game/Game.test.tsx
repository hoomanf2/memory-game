import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import Game from './Game';

import { useFetchCards } from '../../hooks/useFetchCards';
jest.mock('../../hooks/useFetchCards');

describe('Integration test for Game component', () => {
  beforeEach(() => {
    const cards = [];

    for (let i = 0; i < 48; i++) {
      cards.push(i);
    }

    (useFetchCards as jest.Mock).mockReturnValue([
      () => null,
      { loading: false, error: '', cards }
    ]);
  });

  it('renders game component with easy level', async () => {
    render(<Game level={'easy'} />);

    const cardElements = screen.queryAllByTestId('card');
    expect(cardElements.length).toBe(48);
  });

  it('The Play button should be appeared after selecting 4 cards in easy level', async () => {
    render(<Game level={'easy'} />);

    const cardElements = screen.queryAllByTestId('card');

    const leftClick = { button: 0 };
    for (let i = 0; i < 4; i++) {
      fireEvent.click(cardElements[i], leftClick);
      expect(cardElements[i]).toHaveClass('flipped');
    }

    const plyButtonElement = screen.getByTestId('playButton');
    expect(plyButtonElement).toBeInTheDocument();
  });

  it('When you playing, the cards that were not selected in the initial step are not selectable', async () => {
    render(<Game level={'easy'} />);

    const cardElements = screen.queryAllByTestId('card');

    const leftClick = { button: 0 };
    for (let i = 0; i < 4; i++) {
      fireEvent.click(cardElements[i], leftClick);
    }

    const plyButtonElement = screen.getByTestId('playButton');
    fireEvent.click(plyButtonElement, leftClick);

    const chooseCardsMessageElement = screen.getByText(
      /Please choose cards that you selected last time in ascending order/i
    );
    expect(chooseCardsMessageElement).toBeInTheDocument();

    fireEvent.click(cardElements[5], leftClick);
    await waitFor(() => {
      expect(cardElements[5]).not.toHaveClass('flipped');
    });
  });

  it('play a game and losing in game', async () => {
    render(<Game level={'easy'} />);

    const cardElements = screen.queryAllByTestId('card');

    const leftClick = { button: 0 };
    for (let i = 0; i < 4; i++) {
      fireEvent.click(cardElements[i], leftClick);
    }

    const plyButtonElement = screen.getByTestId('playButton');
    fireEvent.click(plyButtonElement, leftClick);

    const chooseCardsMessageElement = screen.getByText(
      /Please choose cards that you selected last time in ascending order/i
    );
    expect(chooseCardsMessageElement).toBeInTheDocument();

    fireEvent.click(cardElements[3], leftClick);

    const playerStatusElement = screen.getByText(/You Lose/i);
    expect(playerStatusElement).toBeInTheDocument();

    const restartButtonElement = screen.getByTestId('restartButton');
    expect(restartButtonElement).toBeInTheDocument();
  });

  it('playing two rounds and history should show two records ordered by date', async () => {
    render(<Game level={'easy'} />);

    // First Round (Win)

    const cardElements = screen.queryAllByTestId('card');

    const leftClick = { button: 0 };
    for (let i = 0; i < 4; i++) {
      fireEvent.click(cardElements[i], leftClick);
    }

    let plyButtonElement = screen.getByTestId('playButton');
    fireEvent.click(plyButtonElement, leftClick);

    let chooseCardsMessageElement = screen.getByText(
      /Please choose cards that you selected last time in ascending order/i
    );
    expect(chooseCardsMessageElement).toBeInTheDocument();

    for (let i = 0; i < 4; i++) {
      fireEvent.click(cardElements[i], leftClick);
    }

    let playerStatusElement = screen.queryByText(/You Win/i);
    expect(playerStatusElement).toBeInTheDocument();

    // Second Round (Lose)

    const restartButtonElement = screen.getByTestId('restartButton');
    fireEvent.click(restartButtonElement, leftClick);

    playerStatusElement = screen.queryByText(/You Win/i);
    expect(playerStatusElement).not.toBeInTheDocument();

    for (let i = 0; i < 4; i++) {
      fireEvent.click(cardElements[i], leftClick);
    }

    plyButtonElement = screen.getByTestId('playButton');
    fireEvent.click(plyButtonElement, leftClick);

    chooseCardsMessageElement = screen.getByText(
      /Please choose cards that you selected last time in ascending order/i
    );
    expect(chooseCardsMessageElement).toBeInTheDocument();

    fireEvent.click(cardElements[3], leftClick);

    playerStatusElement = screen.queryByText(/You Lose/i);
    expect(playerStatusElement).toBeInTheDocument();

    const historyList = screen.getByTestId('historyList');
    expect(historyList.childElementCount).toBe(2);
    expect(historyList.childNodes[0].textContent).toMatch(
      /Status: Lose | Selected Cards: 3/
    );
    expect(historyList.childNodes[1].textContent).toMatch(
      /Status: Win | Selected Cards: 1,2,3,4/
    );
  });
});
