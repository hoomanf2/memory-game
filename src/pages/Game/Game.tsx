import React, { useEffect, useState } from 'react';
import { GameLevelType } from '../../types';
import { useCheckGameStatus } from '../../hooks/useCheckGameStatus';
import { useFetchCards } from '../../hooks/useFetchCards';
import { useGameHistory } from '../../hooks/useGameHistory';
import Alert from '../../components/Alert/Alert';
import Button from '../../components/Button/Button';
import Boards from '../../components/Board/Board';
import History from '../../components/History/History';
import { capitalizeFirstLetter } from '../../utils';
import './Game.css';

interface Props {
  level: GameLevelType;
}

type PlayerStatusType = 'win' | 'lose' | undefined;

function Game({ level }: Props) {
  const [isPlaying, setIsPlaying] = useState(false);
  const [initialSelectedCards, setInitialSelectedCards] = useState<number[]>(
    []
  );
  const [selectedCards, setSelectedCards] = useState<number[]>([]);

  const gameStatus = useCheckGameStatus(initialSelectedCards, selectedCards);
  const history = useGameHistory(gameStatus, selectedCards);
  const [fetchCards, { loading, error, cards }] = useFetchCards();

  const numberOfSelectableCards =
    level === 'easy' ? 4 : level === 'medium' ? 8 : 12;

  let playerStatus: PlayerStatusType;
  if (gameStatus !== undefined) playerStatus = gameStatus ? 'win' : 'lose';

  useEffect(() => {
    fetchCards();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const selectCard = (cardNumber: number) => {
    if (isPlaying) {
      if (!selectedCards.includes(cardNumber))
        setSelectedCards([...selectedCards, cardNumber]);
    } else {
      if (!initialSelectedCards.includes(cardNumber))
        setInitialSelectedCards([...initialSelectedCards, cardNumber]);
    }
  };

  const restartGame = () => {
    fetchCards();
    setIsPlaying(false);
    setInitialSelectedCards([]);
    setSelectedCards([]);
  };

  const cardStatus =
    isPlaying && !playerStatus
      ? 'isPlaying'
      : initialSelectedCards.length === numberOfSelectableCards || playerStatus
      ? 'disabled'
      : null;

  const showChooseCardsMessage =
    !error &&
    !loading &&
    cards.length &&
    initialSelectedCards.length !== numberOfSelectableCards;

  const showPlayButton =
    !isPlaying && initialSelectedCards.length === numberOfSelectableCards;

  return (
    <>
      {!loading && error && (
        <>
          <Alert type="danger">{error}</Alert>
          <Button onClick={() => fetchCards()}>Try Again</Button>
        </>
      )}

      {loading && <Alert type="info">Loading...</Alert>}

      {showChooseCardsMessage ? (
        <Alert type="info">Choose {numberOfSelectableCards} Cards</Alert>
      ) : null}

      {cardStatus === 'isPlaying' && (
        <Alert type="info">
          Please choose cards that you selected last time in ascending order
        </Alert>
      )}

      {showPlayButton && (
        <Button onClick={() => setIsPlaying(true)} data-testid="playButton">
          Play
        </Button>
      )}

      {playerStatus && (
        <>
          <Alert type={playerStatus === 'win' ? 'success' : 'danger'}>
            You {capitalizeFirstLetter(playerStatus)}!
          </Alert>
          <Button onClick={() => restartGame()} data-testid="restartButton">
            Restart
          </Button>
        </>
      )}

      {!error && (
        <>
          <div className={`game${loading ? ' disable' : null}`}>
            <Boards
              cards={cards}
              cardStatus={cardStatus}
              initialSelectedCards={initialSelectedCards}
              selectCard={selectCard}
            />
          </div>
          {history.length || isPlaying ? <History history={history} /> : null}
        </>
      )}
    </>
  );
}

export default React.memo(Game);
