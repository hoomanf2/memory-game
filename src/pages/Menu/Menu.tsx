import React, { useState } from 'react';
import { GameLevelType } from '../../types';
import Header from '../../components/Header/Header';
import SelectLevel from '../SelectLevel/SelectLevel';
import Game from '../Game/Game';

function Menu() {
  const [level, setLevel] = useState<GameLevelType>(null);
  return (
    <>
      <Header level={level} />
      {!level ? <SelectLevel setLevel={setLevel} /> : <Game level={level} />}
    </>
  );
}

export default Menu;
