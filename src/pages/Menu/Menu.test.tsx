import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Menu from './Menu';

import { useFetchCards } from '../../hooks/useFetchCards';
jest.mock('../../hooks/useFetchCards');

describe('Integration test for Menu component', () => {
  beforeEach(() => {
    const cards: number[] = [0];

    (useFetchCards as jest.Mock).mockReturnValue([
      () => null,
      { loading: false, error: '', cards }
    ]);
  });

  it('renders menu component with easy level', async () => {
    render(<Menu />);

    const easyButtonElement = screen.getByText('Easy');
    const leftClick = { button: 0 };
    fireEvent.click(easyButtonElement, leftClick);

    const levelElement = screen.getByText(/EASY/i);
    expect(levelElement).toBeInTheDocument();

    const infoElement = screen.getByText(/Choose 4 Cards/i);
    expect(infoElement).toBeInTheDocument();
  });

  it('renders menu component with medium level', async () => {
    render(<Menu />);

    const easyButtonElement = screen.getByText('Medium');
    const leftClick = { button: 0 };
    fireEvent.click(easyButtonElement, leftClick);

    const levelElement = screen.getByText(/MEDIUM/i);
    expect(levelElement).toBeInTheDocument();

    const infoElement = screen.getByText(/Choose 8 Cards/i);
    expect(infoElement).toBeInTheDocument();
  });

  it('renders menu component with hard level', async () => {
    render(<Menu />);

    const easyButtonElement = screen.getByText('Hard');
    const leftClick = { button: 0 };
    fireEvent.click(easyButtonElement, leftClick);

    const levelElement = screen.getByText(/HARD/i);
    expect(levelElement).toBeInTheDocument();

    const infoElement = screen.getByText(/Choose 12 Cards/i);
    expect(infoElement).toBeInTheDocument();
  });
});
