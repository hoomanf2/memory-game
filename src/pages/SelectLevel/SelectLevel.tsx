import React from 'react';
import { GameLevelType } from '../../types';
import Button from '../../components/Button/Button';

interface Props {
  setLevel(level: GameLevelType): any;
}

function SelectLevel({ setLevel }: Props) {
  return (
    <>
      <h2>Choose Level:</h2>
      <div className="level">
        <Button onClick={() => setLevel('easy')}>Easy</Button>
        <Button onClick={() => setLevel('medium')}>Medium</Button>
        <Button onClick={() => setLevel('hard')}>Hard</Button>
      </div>
    </>
  );
}

export default SelectLevel;
