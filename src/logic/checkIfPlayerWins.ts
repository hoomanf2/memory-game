interface IndexesObject {
  [key: string]: any;
}

export const checkIfPlayerWins = (
  initialArray: number[],
  selectedArray: number[]
): boolean | undefined => {
  // Sort initialArray
  initialArray.sort((a, b) => a - b);

  // Create object from index of initialArray
  const initialArrayIndexes: IndexesObject = {};
  for (let index in initialArray) {
    const key = initialArray[index];
    initialArrayIndexes[key] = index;
  }

  // Create an array from selectedArray based on initialArray indexes
  const selectedArrayIndexes: number[] = [];
  selectedArray.forEach((number) => {
    const key = initialArrayIndexes[number];
    if (key) selectedArrayIndexes.push(parseInt(key));
  });

  // Check if selectedArrayIndexes and initialArray not to be empty
  if (selectedArrayIndexes.length && initialArray.length) {
    // Check the order of the selectedArray index
    let lastIndex = -1;
    for (let index of selectedArrayIndexes) {
      if (index > lastIndex && index === lastIndex + 1) {
        lastIndex = index;
      } else {
        return false;
      }
    }

    // Check if all of initialArray numbers selected
    if (selectedArrayIndexes.length === initialArray.length) return true;
  }
};
