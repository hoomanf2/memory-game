import { checkIfPlayerWins } from './checkIfPlayerWins';

describe('Testing the checkIfPlayerWins', () => {
  it('should return undefined with empty initialArray and selectedArray', () => {
    const initialArray: number[] = [];
    const selectedArray: number[] = [];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeUndefined();
  });

  it('should return undefined with empty initialArray', () => {
    const initialArray: number[] = [];
    const selectedArray = [3, 5, 9];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeUndefined();
  });

  it('should return undefined with empty selectedArray', () => {
    const initialArray = [3, 5, 9];
    const selectedArray: number[] = [];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeUndefined();
  });

  it('should return undefined when no numbers in the selectedArray were not included in initialArray', () => {
    const initialArray = [3, 5, 9];
    const selectedArray = [8, 12, 15];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeUndefined();
  });

  it('should return undefined when selectedArray not included all initialArray numbers', () => {
    const initialArray = [1, 12, 15];
    const selectedArray = [1];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeUndefined();
  });

  it('should return false when selectedArray not included all initialArray numbers in ascending order', () => {
    const initialArray = [1, 12, 15];
    const selectedArray = [1, 15, 12];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeFalsy();
  });

  it('should return false when selectedArray numbers not in ascending order', () => {
    const initialArray = [1, 12, 15];
    const selectedArray = [12, 1, 15];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeFalsy();
  });

  it('should return false when first selectedArray numbers not in ascending order', () => {
    const initialArray = [1, 12, 15];
    const selectedArray = [15];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeFalsy();
  });

  it('should return true when selectedArray numbers were in ascending order', () => {
    const initialArray = [1, 12, 15];
    const selectedArray = [1, 12, 15];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeTruthy();
  });

  it('should return true when selectedArray numbers were in ascending order with extra numbers between them', () => {
    const initialArray = [1, 12, 15];
    const selectedArray = [1, 5, 8, 12, 13, 14, 15];
    const result = checkIfPlayerWins(initialArray, selectedArray);
    expect(result).toBeTruthy();
  });
});
