export type GameLevelType = null | 'easy' | 'medium' | 'hard';

export interface History {
  isWinning: boolean;
  selectedCards: number[];
  dateTime: string;
}
