import { useState, useEffect } from 'react';
import { History } from '../types';

export function useGameHistory(
  gameStatus: boolean | undefined,
  selectedCards: number[]
) {
  const [history, setHistory] = useState<History[]>([]);

  useEffect(() => {
    if (gameStatus !== undefined) {
      setHistory((state) => {
        // A copy of history in state
        const newState = [...state];
        newState.splice(0, 0, {
          isWinning: gameStatus,
          selectedCards,
          dateTime: new Date().toString()
        });
        return newState;
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gameStatus]);

  return history;
}
