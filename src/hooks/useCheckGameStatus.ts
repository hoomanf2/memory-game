import { useMemo } from 'react';
import { checkIfPlayerWins } from '../logic/checkIfPlayerWins';

export const useCheckGameStatus = (
  initialArray: number[],
  selectedArray: number[]
): boolean | undefined => {
  const result = useMemo(() => checkIfPlayerWins(initialArray, selectedArray), [
    initialArray,
    selectedArray
  ]);
  return result;
};
