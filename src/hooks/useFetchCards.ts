import { useState } from 'react';

type FetchCardsType = [
  action: () => Promise<void>,
  data: { loading: boolean; error: string; cards: number[] }
];

export function useFetchCards() {
  const [cards, setCards] = useState([]);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);

  const fetchCards = async () => {
    setLoading(true);
    try {
      const res = await fetch(`${process.env.REACT_APP_API}/getRandomNumber`);
      const json = await res.json();
      if (json.status) {
        setCards(json.randomNumbers);
        setError(null);
      } else setError(json.message); // Returning server error message
      setLoading(false);
    } catch (err) {
      setError(err.message);
      setCards([]);
      setLoading(false);
    }
  };

  return [fetchCards, { loading, error, cards }] as FetchCardsType;
}
