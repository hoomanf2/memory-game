# Memory game

In this memory game, A player based on the game level can choose the number of cards at first, and by selecting each card, It's flipped and, A number revealed on the other side of the card. Then a play button appears and, by clicking on it, the player must select all of the previously chosen cards in ascending order. If the player clicks on a card that he didn't choose at first, It's flipped only for one moment. In other words, the player can only select cards during the game that he has already chosen in the first step.

**Stack**

I used React + TypeScript for the front-end also wrote some tests for game logic and other important components.

For the back-end, I used Express with TypeScript in order to create a single route API 😀

## Available Scripts

First run the server: [Memory Game Server](https://gitlab.com/hoomanf2/memory-game-server/)

Then, In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
